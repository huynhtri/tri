# Tri Ruby Gem

Tri gem is written purely in ruby without any external gem dependencies. The main idea of this gem is to help you create an easy WebSocket protocol server base on the RFC6455 document. You can use HTML5 default WebSocket to communicate with this server.

### Installation

You only need to download this gem from RubyGem.

```sh
$ gem install "tri"
```
or 
```sh
$ gem "tri"
```


### Server Deployment

To start a server with default localhost (127.0.0.1)  address and port 888, you just need a simple code:
```sh
puts Tri::start
```
This function also return some useful information about either the server successfully run or not. To change the default parameter for the server, we add more information to the function as
```sh
puts Tri::start(8080)    # Start server with the default localhost
                         # but different listenning port: 8080
```
```sh
puts Tri::start("192.168.0.1")    # Start server with the default litening 
                                  # port 888 but different ip address
```
```sh
puts Tri::start("192.168.0.1",8080)    # Start server with the ip adress  and
                                       # port 8080
```
To check if server is running or not
```sh
puts Tri::is_running?    # Return TRUE or FALSE
```
Get the WebSocket protocol client
```sh
client1 = Tri::get_client    # Return a client [Socket] or nil
```
The function above does not send PING frame to keep the connection alive; if you want to keep the connection alive, maintain for a long time. Use the next function:
```sh
client1 = Tri::get_client_with_keep_alive(#sec)    
                             # Return a client [Socket] or nil
                             # Server will send PING every #seccond
```
to receive data from client.
```sh
puts Tri::receive_from(client1)    # Return a array type has size of 2
                                   # array[0] is return code number
                                   # array[1] is acual data
```
The return code number is very important because it gives us helpful information to make the next decision. If the return code is:
- {-2} The payload is too big.
- {-1} Waiting too long for the client, but get nothing. This client should be terminated.
- {1} Valid text data
- {8} Client just sent a goodbye frame. This connection should be terminated.
- {9} Client just sent a PING frame. Should sent back a PONG frame with same data.
- {10} Client just just a PONG frame.

This function will cause the current thread to stop waiting until the client finishes sending something. If you would like to separate the listening process, just a thread such as:
```sh
Thread.new { puts Tri::receive_from(client1) }
```
To send something to the client:
```sh
Tri::send_to(client1,@dataNeedToBeSent) # data input is normal string type
```
because the client here is inherited from the Socket class. We can terminate the connection by these commands:
```sh
client1.close         # close the connection in a formal way
client1 = nil         # destroy the object client1
```
### Example code
```sh
Tri::start
while true
    client = Tri::get_client
    while client
            puts Tri::receive_from(client)
            Tri::send_to(client,"I just got the message!")
    end
end
```
### Examples & Projects using Tri gem
- HTML5 WebSocket [example](http://chat4ninja.com/web_socket_example.html)
- HTML5 WebSocket [code](http://chat4ninja.com/web_socket_code.html)
- Chat application pilot project [Chat4ninja](http://chat4ninja.com)

### Contact

Please feel free to reach me at hdtri.expert@gmail.com for free converstaion!

### License

**(The MIT License)**

Copyright © 2016 Tri Huynh

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the ‘Software’), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED ‘AS IS’, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


