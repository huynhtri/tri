Gem::Specification.new do |spec|
  spec.name          = "tri"
  spec.version       = "0.1.0"
  spec.authors       = ["Tri Huynh"]
  spec.email         = ["hdtri.expert@gmail.com"]
  spec.summary       = %q{Simple WebSocket server.}
  spec.description   = %q{Pure ruby code to run a WebSocket server.}
  spec.homepage      = "http://chat4ninja.com"
  spec.license       = "MIT"
 spec.files         = "lib/tri.rb"

end
